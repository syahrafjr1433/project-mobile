import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  armor_titan a = armor_titan();
  attack_titan b = attack_titan();
  beast_titan c = beast_titan();
  human d = human();

  a.levelPoint = 2;
  b.levelPoint = 3;
  c.levelPoint = 4;
  d.levelPoint = 5;

  print("Level point Armor Titan: ${a.levelPoint}");
  print("Level point Attack Titan: ${b.levelPoint}");
  print("Level point Beast Titan: ${c.levelPoint}");
  print("Level point Human: ${d.levelPoint}");

  print("Sifat dari Armor Titan: " + a.terjang());
  print("Sifat dari Attack Titan: " + b.punch());
  print("Sifat dari Beast Titan: " + c.lempar());
  print("Sifat dari Human : " + d.killAlltitan());
}

